package org.example;

import org.example.framework.Context;
import org.example.model.Bike;
import org.example.model.Car;

public class Main {
    public static void main(String[] args) {
        Context context = Context.load("org.example.model");
        System.out.println(context.getLoadedClasses());

        try {
            Car car = (Car) context.get("Car");
            System.out.println(car.toString());
            Bike bike = (Bike) context.get("Bike");
            System.out.println(bike.toString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }
}