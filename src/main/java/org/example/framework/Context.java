package org.example.framework;

import org.example.annotations.Autowired;
import org.example.annotations.Component;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Context {
    private Map<String, Class<?>> loadedClasses;

    private Context(Map<String, Class<?>> loadedClasses) {
        this.loadedClasses = loadedClasses;
    }

    public static Context load(String packageName) {
        Reflections reflections = new Reflections(packageName, new SubTypesScanner(false));
        Map<String, Class<?>> clazzes = reflections.getSubTypesOf(Object.class).stream().filter(clazz -> clazz.isAnnotationPresent(Component.class)).collect(Collectors.toMap(clazz -> clazz.getAnnotation(Component.class).value(), clazz -> clazz));

        return new Context(clazzes);
    }

    public Map<String, Class<?>> getLoadedClasses() {
        return loadedClasses;
    }

    public Object get(String className) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        if (!loadedClasses.containsKey(className)) {
            throw new RuntimeException("Нет такого объекта");
        }

        Class<?> clazz = loadedClasses.get(className);
        var constructors = clazz.getDeclaredConstructors();
        var annotatedConstructor = Arrays.stream(constructors)
                .filter(con -> con.isAnnotationPresent(Autowired.class))
                .findFirst();

        if (annotatedConstructor.isPresent()) {
            return ConstructorAutowired(annotatedConstructor);
        } else {
            return fieldAutowired(clazz, constructors);
        }
    }

    public Object ConstructorAutowired(Optional<Constructor<?>> annotatedConstructor) throws InvocationTargetException, InstantiationException, IllegalAccessException {
        var constructor = annotatedConstructor.get();
        var parameterTypes = constructor.getParameterTypes();
        var params = Arrays.stream(parameterTypes)
                .map(
                        cl -> {
                            try {
                                return get(cl.getAnnotation(Component.class).value());
                            } catch (Exception e) {
                                throw new RuntimeException("Такой тип нельзя подсавлять как параметр");
                            }
                        }

                ).collect(Collectors.toList());
        return constructor.newInstance(params.toArray());
    }

    public Object fieldAutowired(Class<?> clazz, Constructor<?>[] constructors) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Field[] field = clazz.getDeclaredFields();
        boolean flag = false;
        var constructor = Arrays.stream(constructors)
                .filter(x -> x.getParameterTypes()
                        .length == field.length).findFirst();
        if (constructor.isPresent()) {
            return ConstructorAutowired(constructor);
        }
        Object clazzAutowiredField = clazz.getConstructor().newInstance();
        var autowiredField = Arrays.stream(field)
                .filter(x -> x.isAnnotationPresent(Autowired.class)).toList();
        if (!autowiredField.isEmpty()) {
            autowiredField.stream()
                    .forEach(x -> {
                        x.setAccessible(true);
                        try {
                            x.set(clazzAutowiredField,
                                    get(x.getType().getAnnotation(Component.class).value()));
                        } catch (IllegalAccessException
                                 | NoSuchMethodException
                                 | InvocationTargetException
                                 | InstantiationException e) {
                            throw new RuntimeException(e);
                        }
                    });
        }
        return clazzAutowiredField;
    }

}
