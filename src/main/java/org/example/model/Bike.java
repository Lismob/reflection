package org.example.model;

import org.example.annotations.Autowired;
import org.example.annotations.Component;

@Component("Bike")
public class Bike {
    @Autowired
    private Body body;
    @Autowired
    private Engine engine;

    public Bike() {

    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String toString() {
        return "Bike{" +
                "body=" + body +
                ", engine=" + engine +
                '}';
    }
}

